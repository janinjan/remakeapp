//
//  TodayViewController.swift
//  RemakeApp2
//
//  Created by Janin Culhaoglu on 12/12/2019.
//  Copyright © 2019 Janin Culhaoglu. All rights reserved.
//

import UIKit

class TodayViewController: UIViewController {

    // MARK: - Properties
    var imagesArrayCell1: [UIImage] = [#imageLiteral(resourceName: "image1"), #imageLiteral(resourceName: "image3")]
    var imagesArrayCell2: [UIImage] = [#imageLiteral(resourceName: "image2"), #imageLiteral(resourceName: "image4")]
    let segueIdentifier = "todayToDetail"

    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "TodayCVCell2", bundle: nil), forCellWithReuseIdentifier: "cell2")
        collectionView.register(UINib(nibName: "TodayCVCell1", bundle: nil), forCellWithReuseIdentifier: "cell1")
    }
}

// MARK: - UICollectionView Datasource
extension TodayViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as? TodayCVCell1 else { return UICollectionViewCell() }
        guard let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as? TodayCVCell2 else { return UICollectionViewCell() }

        switch indexPath.section {
        case 0:
            switch indexPath.item {
            case 0:
                cell1.title.text = "Explain Eveything Whiteboard"
                cell1.descriptionLabel.text = "Share ideas with anyone, anytime"
                cell1.todayImageView.image = imagesArrayCell1[0]
                return cell1
            case 1:
                cell2.categoryName.text = "DIY"
                cell2.title.text = "Edit your live Photos"
                cell2.todayImageViewCell2.image = imagesArrayCell2[0]
                return cell2
            case 2:
                cell1.title.text = "Pinterest: Lifestyle Ideas"
                cell1.descriptionLabel.text = "Create and share your boards"
                cell1.todayImageView.image = imagesArrayCell1[1]
                return cell1
            case 3:
                cell2.categoryName.text = "STAR POWER"
                cell2.title.text = "Sing your heart out"
                cell2.todayImageViewCell2.image = imagesArrayCell2[1]
                return cell2
            default:
                return UICollectionViewCell()
            }
        default:
            return UICollectionViewCell()
        }
    }
}

// MARK: - UICollectionView Delegate
extension TodayViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
}

// MARK: - CollectionView Delegate FlowLayout

extension TodayViewController: UICollectionViewDelegateFlowLayout {
    // Item spacing
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
}
