//
//  TodayCVCell1.swift
//  RemakeApp2
//
//  Created by Janin Culhaoglu on 12/12/2019.
//  Copyright © 2019 Janin Culhaoglu. All rights reserved.
//

import UIKit

class TodayCVCell1: UICollectionViewCell {

    @IBOutlet weak var todayImageView: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var whiteView: UIView!

    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set {
            if newValue {
                super.isSelected = true
                
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .beginFromCurrentState, animations: {
                    self.contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.96, y: 0.96)
                }, completion: {
                    finished in
                    UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .beginFromCurrentState, animations:  {
                        self.contentView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                    }, completion: nil)
                })
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        addShadowsToCell()
    }

    /// Add shadows and border to cells
    private func addShadowsToCell() {
        contentView.layer.cornerRadius = 8.0
        whiteView.layer.cornerRadius = 8.0
        whiteView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        todayImageView.layer.cornerRadius = 8.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = false
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 1.0
        layer.masksToBounds = false
    }
}
